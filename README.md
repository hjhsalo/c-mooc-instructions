     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 


Ohjeet Cloud9-työtilan käyttämiseksi [_"MOOC C"_-kurssilla](http://mooc.fi/courses/2016/aalto-c/).

### Asenna tehtävien paikalliseen tarkastamiseen tarvittavat ohjelmistot ([libcheck -kirjasto](https://libcheck.github.io/check/)):

    $ sudo apt-get update
    $ sudo apt-get install check

### Asenna TMC-komentorivityökalut (TestMyCode)

#### 1. Asenna virtualenv (tmc.py:n installer vaatii tämän)

    $ sudo pip install virtualenv

#### 2. Lataa ja aja tmc.py:n asennusskripti

    $ sudo curl -L https://rawgit.com/PasiSa/tmc.py/master/tmcpy-inst.sh | bash

#### 3. Rekisteröi tmc-sovelluksen asennuskansio PATH-ympäristömuuttujaan:

    $ export PATH=$PATH:$HOME/.local/bin

##### TMC-komentorivityökalujen dokumentaatiota:
https://juhaniimberg.github.io/tmc.py/


### TMC-komentorivityökalun konfigurointi:

#### 0. Käynnistä avustettu konfigurointi

    $ tmc configure

#### 1. Syötä käytettävän TMC-palvelimen URL-osoite.

    https://tmc.mooc.fi/mooc

#### 2. Syötä käyttäjätunnuksesi
Jos sinulla ei ole käyttäjätunnusta niin tee se täällä:
https://tmc.mooc.fi/mooc

#### 3. Syötä salasanasi

#### 4. Syötä kansio mihin kurssin tehtävät ladataan:

    /home/ubuntu/workspace/tmc/2016-aalto-c

#### 5. Valitse mitkä kaikki tehtävät ladataan. (Lataa kaikki)

    a

Happy coding!

### Tehtävien tekeminen
HUOM!: Tehtävät on jaoteltu tehtäväkokonaisuuksiin.

1. Valitse tehtävä jonka haluat tehdä


    $ tmc select

2. Lue tehtävän ohjeet
Tehtävän ohjeet löytyvät tehtävän kansion juuresta.
Aukaise (tai esikatsele) desc-<n>-fi.md missä <n> on sen tehtävän numero minkä ohjeita haluat tarkastella.

3. Tee tehtävä ohjeiden mukaisesti


4. Aja paikalliset testit


    $ tmc test

5. Palauta tehtävä
NTS / TODO: "Selected "Module_1-02_intro" based on the current directory."
--> Onko nyt niin että "tmc select"-komento ei vaikuta "tmc submit"-komentoon?


    $ tmc submit
    Selected "Module_1-02_intro" based on the current directory.
    Submitting from: /home/ubuntu/workspace/tmc/2016-aalto-c/Module_1/02_intro
    Module_1-02_intro -> TMC Server
    Submission has been sent.                       
    Results:            
    All tests successful!
    ---------------------
    Points [M1.02.1, M1.02.2]
    Submission URL: https://tmc.mooc.fi/mooc/submissions/1342400


### Paikallisten testien ajaminen
HUOM!: komento "tmc test" ajaa kaikki test-kansiosta löytyvät testit

1. Valitse tehtäväkokonaisuus, jonka haluat paikalliset testit haluat ajaa


    $ tmc select

2. Aja tehtäväkokonaisuuden paikalliset testit


    $ tmc test
    
### Tehtävien palauttaminen
TODO

### Uusien tehtävien lataaminen
TODO

### git-versionhallinnan käyttö
TODO


### TMC-komentorivityökalujen tulostusvärin muuttaminen
TODO (Nyt "tmc test"-komennon virheviesteistä hankala saada selvää; tummanpunainen teksti sinisellä taustalla)


### Tuki ja lisäohjeet

Visit http://docs.c9.io for support, or to learn more about using Cloud9 IDE. 
To watch some training videos, visit http://www.youtube.com/user/c9ide
